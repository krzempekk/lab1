package agh.cs.lab1;

import static java.lang.System.out;

public class World {
    private static void run(Direction[] dirs) {
        for(Direction dir : dirs) {
            switch(dir) {
                case FORWARD:
                    out.println("Creature goes forwards");
                    break;
                case BACKWARD:
                    out.println("Creature goes backwards");
                    break;
                case RIGHT:
                    out.println("Creature goes right");
                    break;
                case LEFT:
                    out.println("Creature goes left");
                    break;
            }
        }
    }

    private static Direction[] convert(String[] args) {
        Direction[] dirs = new Direction[args.length];
        for(int i = 0; i < args.length; i++) {
            switch(args[i]) {
                case "f":
                    dirs[i] = Direction.FORWARD;
                    break;
                case "b":
                    dirs[i] = Direction.BACKWARD;
                    break;
                case "r":
                    dirs[i] = Direction.RIGHT;
                    break;
                case "l":
                    dirs[i] = Direction.LEFT;
                    break;
                default:
                    dirs[i] = Direction.NOWHERE;
            }
        }
        return dirs;
    }

    public static void main(String[] args) {
        out.println("System started");
        Direction[] dirs = convert(args);
        run(dirs);
        out.println("System finished");
    }
}
